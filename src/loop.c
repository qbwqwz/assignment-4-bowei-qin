//
//  loop.c
//  
//
//  Created by Paul.Q on 5/13/15.
//
//

#include "loop.h"
#include <stdio.h>
#include <R.h>


void bluemove(int *index, int *grid, int *r, int *numindex, int *m)
{
    int i, j, k;
    int index_m[*numindex];
    
    for (i=0; i < *numindex; i++){
        if(index[i] % (*r) == 0){
            index_m[i] = index[i] - *r + 1;
        } else {
            index_m[i] = index[i] + 1;
        }
    }
    *m = 0;
    for(j=0; j < *numindex; j++){
        if(grid[index_m[j] - 1] != 0){
            index_m[j] = index[j];
        }else {
            (*m)++;
        }
    }
    for (k=0; k < *numindex; k++){
        index[k] = index_m[k];
    }
}

void redmove(int *index, int *grid, int *r, int *c, int *numindex, int *m)
{
    int i, j, k;
    int index_m[*numindex];
    
    for (i=0; i < *numindex; i++){
        if(index[i] > (*r) * (*c - 1)){
            index_m[i] = index[i] + *r - (*r)*(*c);
        } else {
            index_m[i] = index[i] + *r;
        }
    }
    *m = 0;
    for(j=0; j < *numindex; j++){
        if(grid[index_m[j] - 1] != 0){
            index_m[j] = index[j];
        }else{
            (*m)++;
        }
    }
    for(k=0; k < *numindex; k++){
        index[k] = index_m[k];
    }
}

void loop(int *numSteps, int *r, int *c, int *m, int *grid, int *index_b, int *index_r, int *numindex, int *numgrid, int *mv)
{
    int i, j, k, l, x, y, z;
    
    for(i=0; i < *numSteps; i++){
        if(i % 2 == 0){
            bluemove(index_b, grid, r, numindex, m);
            for(j=0; j < *numgrid; j++){
                grid[j] = 0;
            }
            for(k=0; k < *numindex; k++){
                grid[index_b[k] - 1] = 1;
            }
            for(l=0; l < *numindex; l++){
                grid[index_r[l] - 1] = 2;
            }
            mv[i] = *m;
        }
        if(i % 2 == 1){
            redmove(index_r, grid, r, c, numindex, m);
            for(x=0; x < *numgrid; x++){
                grid[x] = 0;
            }
            for(y=0; y < *numindex; y++){
                grid[index_b[y] - 1] = 1;
            }
            for(z=0; z < *numindex; z++){
                grid[index_r[z] - 1] = 2;
            }
            mv[i] = *m;
        }
    }
}










